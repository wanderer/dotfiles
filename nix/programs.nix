{
  config,
  pkgs,
  ...
}: {
  imports = [
    ./programs/helix.nix
    ./programs/newsboat.nix

    ./cli.nix
  ];
}
