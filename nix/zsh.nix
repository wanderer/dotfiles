{
  config,
  pkgs,
  ...
}: {
  home.file = {
    ".zshrc" = {
      source = ../.zshrc;
    };
    ".zshenv" = {
      source = ../.zshenv;
    };
    ".zprofile" = {
      source = ../.zprofile;
    };
    ".zsh" = {
      source = ../.zsh;
      recursive = true;
    };
    ".zsh/bemenu-dracula" = {
      source = pkgs.fetchFromGitHub {
        owner = "dracula";
        repo = "bemenu";
        rev = "9b1165b3d97e3b2a74c6ce220781b78d8a11febf";
        sha256 = "sha256-TwfkEZ1aTkHur+jCqRsaqvzOw6qpH0L4pvYqkx7iCDk=";
      };
    };
  };
}
