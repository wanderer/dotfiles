{
  config,
  pkgs,
  hostName,
  ...
}: let
  catppuccin = import ./themes/catppuccin.nix {inherit pkgs;};
in {
  configFile."kitty/config.d" = {
    source = ../.config/kitty/config.d;
  };

  # set current kitty theme using:
  #   kitty +kitten themes --reload-in all <theme>
  #
  # this makes linking the custom dracula to current-theme.conf obsolete

  # configFile."kitty/current-theme.conf" = {
  configFile."kitty/themes/customDracula.conf" = {
    source = ../.config/kitty/customDracula.conf;
  };
  configFile."kitty/themes/diff-frappe.conf" = {
    source =
      catppuccin.kitty
      + "/themes/diff-frappe.conf";
  };
  configFile."kitty/themes/diff-latte.conf" = {
    source =
      catppuccin.kitty
      + "/themes/diff-latte.conf";
  };
  configFile."kitty/themes/diff-macchiato.conf" = {
    source =
      catppuccin.kitty
      + "/themes/diff-macchiato.conf";
  };
  configFile."kitty/themes/diff-mocha.conf" = {
    source =
      catppuccin.kitty
      + "/themes/diff-mocha.conf";
  };
  configFile."kitty/themes/frappe.conf" = {
    source =
      catppuccin.kitty
      + "/themes/frappe.conf";
  };
  configFile."kitty/themes/latte.conf" = {
    source =
      catppuccin.kitty
      + "/themes/latte.conf";
  };
  configFile."kitty/themes/macchiato.conf" = {
    source =
      catppuccin.kitty
      + "/themes/macchiato.conf";
  };
  configFile."kitty/themes/mocha.conf" = {
    source =
      catppuccin.kitty
      + "/themes/mocha.conf";
  };

  configFile."bat/themes/Catppuccin-latte.tmTheme" = {
    source =
      catppuccin.bat
      + "/Catppuccin-latte.tmTheme";
  };
  configFile."bat/themes/Catppuccin-frappe.tmTheme" = {
    source =
      catppuccin.bat
      + "/Catppuccin-frappe.tmTheme";
  };
  configFile."bat/themes/Catppuccin-macchiato.tmTheme" = {
    source =
      catppuccin.bat
      + "/Catppuccin-macchiato.tmTheme";
  };
  configFile."bat/themes/Catppuccin-mocha.tmTheme" = {
    source =
      catppuccin.bat
      + "/Catppuccin-mocha.tmTheme";
  };

  configFile."btop/themes/catppuccin_mocha.theme" = {
    source =
      catppuccin.btop
      + "/themes/catppuccin_mocha.theme";
  };

  configFile."aerc/aerc.conf" = {
    # source = ../.config/aerc/aerc.conf;
    source = ../.config/aerc/aerc-${hostName}.conf;
  };
  configFile."aerc/binds.conf" = {
    source = ../.config/aerc/binds.conf;
  };

  configFile."mako/dracula-mako" = {
    source = pkgs.fetchFromGitHub {
      owner = "dracula";
      repo = "mako";
      rev = "80caf30d59f8017e306519ca33609be75a86ebaf";
      sha256 = "sha256-qoQODX2435xfH9G6xOVFXitKy/75MaDU+tiQ/EsjRV4=";
    };
  };

  configFile."sheldon/plugins.toml" = {
    source = ../.config/sheldon/plugins.toml;
  };

  configFile."starship.toml" = {
    source = ../.config/starship.toml;
  };

  configFile."zathura/zathurarc" = {
    source = ../.config/zathura/zathurarc;
  };
  configFile."zathura/dracula-zathura" = {
    source = pkgs.fetchFromGitHub {
      owner = "dracula";
      repo = "zathura";
      rev = "b597b1537aa125e8829bef2cc57a0b0c6a6b35a1";
      sha256 = "sha256-g6vxwPw0Q9QFJBc3d4R3ZsHnnEvU5o1f4DSuyLeN5XQ=";
    };
  };
  configFile."zathura/catppuccin/latte" = {
    source =
      catppuccin.zathura
      + "/src/catppuccin-latte";
  };
  configFile."zathura/catppuccin/frappe" = {
    source =
      catppuccin.zathura
      + "/src/catppuccin-frappe";
  };
  configFile."zathura/catppuccin/macchiato" = {
    source =
      catppuccin.zathura
      + "/src/catppuccin-macchiato";
  };
  configFile."zathura/catppuccin/mocha" = {
    source =
      catppuccin.zathura
      + "/src/catppuccin-mocha";
  };

  configFile."qutebrowser/dracula" = {
    source = pkgs.fetchFromGitHub {
      owner = "dracula";
      repo = "qutebrowser-dracula-theme";
      rev = "ba5bd6589c4bb8ab35aaaaf7111906732f9764ef";
      sha256 = "sha256-av6laQezAOrBt6P+F2eHWFqAnTEENfDrvzEfhn2dDNY=";
    };
  };
  configFile."qutebrowser/stylesheets" = {
    source = ../.config/qutebrowser/stylesheets;
  };
  configFile."qutebrowser-work/config/stylesheets" = {
    source = ../.config/qutebrowser/stylesheets;
  };

  configFile."ccache/ccache.conf" = {
    source = ../.config/ccache/ccache.conf;
  };

  configFile."go/env" = {
    source = ../.config/go/env;
  };

  configFile."glow/glow.yml" = {
    source = ../.config/glow/glow.yml;
  };

  configFile."gtk-4.0/settings.ini" = {
    source = ../.config/gtk-4.0/settings.ini;
  };
  # configFile."gtk-4.0/gtk.css" = {
    # source = ../.config/gtk-4.0/gtk.css;
  # };

  configFile."environment.d/sway-session.conf" = {
    source = ../.config/environment.d/sway-session.conf;
  };
  configFile."environment.d/java.conf" = {
    source = ../.config/environment.d/java.conf;
  };
  configFile."environment.d/mozilla.conf" = {
    source = ../.config/environment.d/mozilla.conf;
  };
  configFile."environment.d/qt.conf" = {
    source = ../.config/environment.d/qt.conf;
  };
  configFile."zellij/config.kdl" = {
    text = ''
      theme "catppuccin-mocha" // or latte, frappe and macchiato
      keybinds {
        shared {
          bind "Ctrl q" { }
        }
      }
    '';
  };
}
