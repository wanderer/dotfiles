{
  config,
  pkgs,
  ...
}: {
  programs.newsboat = {
    enable = true;
    maxItems = 10000;
    autoReload = true;
    reloadTime = 10; # minutes
    urls = [
      {
        title = "drewdevault's blog";
        url = "https://drewdevault.com/blog/index.xml";
      }
      {
        title = "The Go Blog";
        url = "https://blog.golang.org/feed.atom";
      }
      {
        title = "emersion";
        url = "https://emersion.fr/blog/atom.xml";
        tags = [
          "go"
          "sourcehut"
        ];
      }
      {
        title = "sockpupet";
        url = "https://sockpuppet.org/atom.xml";
      }
      {
        title = "latacora";
        url = "https://latacora.micro.blog/feed.xml";
      }
      {
        title = "archlinux news";
        url = "https://archlinux.org/feeds/news/";
      }
      {url = "https://torresjrjr.com/archive/index.xml";}
      {url = "https://gitea-open-letter.coding.social/updates/atom.xml";}
      {url = "https://www.kalzumeus.com/feed.xml";}
      {url = "https://senan.xyz/feed.xml";}
      {url = "https://www.paritybit.ca/feed.xml";}
      {url = "https://マリウス.com/index.xml";}
      {url = "http://nullr0ute.com/feed/";}
      {url = "https://yurichev.org/rss.xml";}
      {
        url = "https://sourcehut.org/blog/index.xml";
        tags = ["sourcehut"];
      }
      {url = "https://openwrt.org/feed.php?type=rss&mode=list&sort=date&ns=advisory&linkto=current&content=html";}
      {url = "https://sfconservancy.org/feeds/blog/";}
      {
        url = "http://feeds.bbci.co.uk/news/rss.xml";
        tags = ["news"];
      }
      {
        url = "https://www.wired.com/feed/rss";
        tags = ["news"];
      }
    ];
  };
}
