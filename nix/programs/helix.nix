{
  config,
  pkgs,
  ...
}: {
  programs.helix = {
    enable = true;
    settings = {
      theme = "dracula";
      editor = {
        line-number = "relative";
        cursor-shape = {
          insert = "bar";
          normal = "block";
        };
        whitespace = {
          render = "all";
          # or control each character
          #render = {
          #  space = "all";
          #  tab = "all";
          #  newline = "none";
          #};

          characters = {
            space = "·";
            nbsp = "⍽";
            tab = "→";
            newline = "¬";
            # Tabs will look like "→···" (depending on tab width)
            tabpad = "·";
          };
        };
        lsp.display-messages = true;
      };
    };
  };
}
