{pkgs, ...}: let
  catppuccin = {
    kitty = pkgs.fetchFromGitea {
      domain = "gitea.catppuccin.com";
      owner = "catppuccin";
      repo = "kitty";
      rev = "4820b3ef3f4968cf3084b2239ce7d1e99ea04dda";
      sha256 = "sha256-uZSx+fuzcW//5/FtW98q7G4xRRjJjD5aQMbvJ4cs94U=";
    };
    bat = pkgs.fetchFromGitea {
      domain = "gitea.catppuccin.com";
      owner = "catppuccin";
      repo = "bat";
      rev = "ba4d16880d63e656acced2b7d4e034e4a93f74b1";
      sha256 = "sha256-6WVKQErGdaqb++oaXnY3i6/GuH2FhTgK0v4TN4Y0Wbw=";
    };
    btop = pkgs.fetchFromGitea {
      domain = "gitea.catppuccin.com";
      owner = "catppuccin";
      repo = "btop";
      rev = "89ff712eb62747491a76a7902c475007244ff202";
      sha256 = "sha256-J3UezOQMDdxpflGax0rGBF/XMiKqdqZXuX4KMVGTxFk=";
    };
    zathura = pkgs.fetchFromGitea {
      domain = "gitea.catppuccin.com";
      owner = "catppuccin";
      repo = "zathura";
      rev = "d85d8750acd0b0247aa10e0653998180391110a4";
      sha256 = "sha256-5Vh2bVabuBluVCJm9vfdnjnk32CtsK7wGIWM5+XnacM=";
    };
  };
in
  catppuccin
