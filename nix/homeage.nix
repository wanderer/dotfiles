{
  config,
  pkgs,
  ...
}: {
  homeage = {
    # Absolute path to identity (created out of band, not through home-manager)
    identityPaths = [
      "~/.ssh/theEd"
    ];

    # "activation" if system doesn't support systemd
    installationType = "systemd";

    file."sops-age-keys.txt" = {
      # Path to encrypted file tracked by the git repository
      source = ../secrets/sops-keys.age;
      # can be "copies" or "symlink"
      symlinks = [".config/sops/age/keys.txt"];
    };

    file."envs" = {
      source = ../secrets/envs.age;
    };

    # infra secrets.
    file."infra-backend" = {
      source = ../secrets/infra-backend.age;
    };
    file."infra-vars" = {
      source = ../secrets/infra-vars.age;
    };

    file."pcmt_gitea_token" = {
      source = ../secrets/pcmt_gitea_token.age;
    };

    file."hibp_api_key" = {
      source = ../secrets/hibp_api_key.age;
    };
  };
}
