{
  config,
  pkgs,
  ...
}: {
  programs.direnv = {
    enable = true;
    nix-direnv.enable = false;
    stdlib = ''
      if [ -f ''$HOME/.nix-profile/share/nix-direnv/direnvrc ]; then
        source ''$HOME/.nix-profile/share/nix-direnv/direnvrc
      fi

      # as per https://github.com/direnv/direnv/wiki/Vim
      add_extra_vimrc() {
        local extravim="''$(find_up .vimrc)"
        if [ -n "''$extravim" ]; then
          echo "Adding extra .vimrc: ''${extravim}"
          path_add EXTRA_VIM ''$extravim
        fi
      }
    '';
  };
}
