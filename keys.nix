# public ssh keys
{...}: rec {
  surtur = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBtG6NCgdLHX4ztpfvYNRaslKWZcl6KdTc1DehVH4kAL";
  rluks = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIODmLwtQj6ylgdTPo1/H5jW7jsLzwaCTGdIsTQAdc896";
  tflab = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJ9JEzfMs+O6I5JYRQ+gHWClvCqaNTdop8ncDeSj+RWs";

  all = [surtur rluks tflab];
}
