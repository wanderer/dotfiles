{
  config,
  lib,
  pkgs,
  ...
}: let
  hostName = "t14";
  attic = builtins.fetchTarball {
    url = "https://github.com/zhaofengli/attic/tarball/main";
    sha256 = "13h7gqai2lbsp0qqp1spv76fjjsha2lygpvzxk7vzl09h9hnhy5a";
  };
in {
  home.username = "mko";
  home.sessionVariables.HOSTNAME = "${hostName}";
  home.homeDirectory = "/home/${config.home.username}";
  home.stateVersion = "23.11";

  # build configuration and switch:
  # ➜ home-manager switch --no-out-link -b backup --flake~/src/dotfiles#$HOST

  # alternatively, install HM with:
  #   nix profile install --priority 0 home-manager
  # hit the issue described here, waiting until resolved:
  #   https://github.com/nix-community/home-manager/issues/2848
  programs.home-manager.enable = true;

  home.packages = with pkgs; [
    # TODO: configure with HM:
    # zellij, btop, thunderbird, fusuma, swayosd
    direnv
    # lorri and arion are apparently provided by cachix#devenv
    alejandra
    statix
    niv
    rnix-lsp
    deploy-rs
    nixos-rebuild

    eza
    ripgrep
    starship
    sheldon
    duf
    dua
    du-dust
    himalaya
    b3sum
    cargo-watch
    zellij
    cloak
    headscale
    bottom
    btop
    sops
    neovim

    nautilus-open-any-terminal
    sshfs
    asciinema

    dhall
    ccache
    nerdfix

    skate
    pop
    gum
    melt
    wishlist
    # vhs

    # zathura
    autotiling
    bemenu
    swayr
    kanshi
    waybar
    albert
    j4-dmenu-desktop
    sway
    sheldon

    lsb-release
    coreutils
    python3
  ];

  # programs.home-manager.useGlobalPkgs = true;
  # public key: age18rr2vkq3hnhkkju4yktfdwwlsjhwy0drnlppxxkxf5cvgrch0cns27qxkg

  imports = [
    ./nix/programs.nix
    ./nix/qutebrowser-userscripts.nix
    ./nix/scripts.nix

    ./nix/systemd.nix

    ./nix/zsh.nix
  ];

  editorconfig.settings = {
    "*" = {
      charset = "utf-8";
      end_of_line = "lf";
      trim_trailing_whitespace = true;
      insert_final_newline = true;
      max_line_width = 78;
      indent_style = "space";
      indent_size = 4;
    };
  };

  services = {
    kdeconnect = {
      enable = true;
      indicator = true;
    };

    batsignal = {
      enable = true;
      extraArgs = ["-w 25" "-c 15" "-d 10"];
    };

    avizo.enable = true;
  };

  home.file = {
    ".config/kitty/kitty.conf" = {
      source = .config/kitty/kitty.conf;
    };

    ".vimrc" = {
      source = ./.vim/vimrc;
    };
    ".vim/deoplete.vimrc.vim" = {
      source = ./.vim/deoplete.vimrc.vim;
    };
    ".vim/gotags.vimrc.vim" = {
      source = ./.vim/gotags.vimrc.vim;
    };
    ".vim/python.vimrc.vim" = {
      source = ./.vim/python.vimrc.vim;
    };

    ".config/nvim/init.vim" = {
      source = ./.config/nvim/init.vim;
    };

    ".config/systemd/user.conf" = {
      text = ''
        [Manager]
        DefaultTimeoutStarSec=15s
        DefaultTimeoutStopSec=15s
      '';
    };

    ".config/sway/config" = {
      source = ./.config/sway/config;
    };
    ".config/sway/env" = {
      source = ./.config/sway/env;
    };
    ".config/sway/inputs" = {
      source = ./.config/sway/inputs;
    };
    ".config/sway/config.d/dracula.sway" = {
      source = ./.config/sway/config.d/dracula.sway;
    };
    ".config/waybar/config" = {
      source = ./.config/waybar/config;
    };
    ".config/waybar/style.css" = {
      source = ./.config/waybar/style.css;
    };
    ".config/waybar/modules/storage.sh" = {
      source = ./.config/waybar/modules/storage.sh;
    };
    ".config/kanshi/config" = {
      source = ./.config/kanshi/config;
    };
    ".config/mako/config" = {
      source = ./.config/mako/config;
    };
    ".config/swaylock/config" = {
      source = .config/swaylock/config;
    };
    ".local/bin/swws.py" = {
      source = ./bin/swws.py;
    };
    ".local/bin/sway-locker" = {
      source = ./bin/sway-locker;
    };

    ".config/albert.conf" = {
      source = ./.config/albert/albert.conf;
    };

    ".config/direnv/direnv.toml" = {
      source = ./.config/direnv/direnv.toml;
    };

    ".config/fusuma/config-wl.yml" = {
      source = ./.config/fusuma/config-wl.yml;
    };
    ".config/fusuma/config.yml" = {
      source = ./.config/fusuma/config.yml;
    };

    ".config/git/config-common" = {
      source = ./.config/git/config;
    };
    # host-specific gitconfig.
    ".config/git/config.${hostName}" = {
      text = ''
        [user]
            name = ${hostName}
            email = wanderer@dotya.ml
            signingkey = ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDAqwhsHHLKG7k7kDa7Tauy2V+rXqQFxiuMCdVltz0n1
      '';
    };
    ".config/git/config" = {
      text = ''
        [include]
          path = ~/.config/git/config-common
        [include]
          path = ~/.config/git/config.${hostName}
      '';
    };
    ".config/git/allowed_signers" = {
      source = ./.config/git/allowed_signers;
    };

    ".cargo/config.toml" = {
      source = .cargo/config.toml;
    };

    ".npmrc" = {
      text = ''
        prefix=''${HOME}/.npm-packages
        audit=false
        fund=false
      '';
    };

    ".config/tridactyl/tridactylrc" = {
      source = .config/tridactyl/tridactylrc;
    };

    ".config/bat/config" = {
      source = .config/bat/config;
    };

    ".ncpamixer.conf" = {
      source = .config/ncpamixer.conf;
    };

    ".gdbinit" = {
      text = ''
        set auto-load safe-path /nix/store
        set history save on
        set history size 10000
        set history remove-duplicates 100
        set history filename ~/.gdb_history
      '';
    };

    ".searchsploit_rc" = {
      text = ''
        ##-- Program Settings
        progname="$( basename "$0" )"


        ##-- Exploits
        files_array+=("files_exploits.csv")
        path_array+=("''${HOME}/utils/exploit-database")
        name_array+=("Exploit")
        git_array+=("https://gitlab.com/exploit-database/exploitdb.git")
        package_array+=("exploitdb")


        ##-- Shellcodes
        files_array+=("files_shellcodes.csv")
        path_array+=("''${HOME}/utils/exploit-database")
        name_array+=("Shellcode")
        git_array+=("https://gitlab.com/exploit-database/exploitdb.git")
        package_array+=("exploitdb")


        # ##-- Papers
        # files_array+=("files_papers.csv")
        # path_array+=("''${HOME}/utils/exploitdb-papers")
        # name_array+=("Paper")
        # git_array+=("https://gitlab.com/exploit-database/exploitdb-papers.git")
        # package_array+=("exploitdb-papers")
      '';
    };

    ".local/bin/battery.sh" = {
      source = bin/battery.sh;
      executable = true;
    };
    ".local/bin/localbsync" = {
      source = bin/localbsync;
      executable = true;
    };
    ".local/bin/parec-wr" = {
      source = bin/parec-wr;
      executable = true;
    };
    ".local/bin/pscbg" = {
      source = bin/pscbg;
      executable = true;
    };
    ".local/bin/qst_up" = {
      source = bin/qst_up;
      executable = true;
    };
    ".local/bin/winprint.sh" = {
      source = bin/winprint.sh;
      executable = true;
    };

    ".config/qutebrowser/config.py" = {
      source = .config/qutebrowser/config.py;
    };

    ".config/qutebrowser/greasemonkey/DR.js" = {
      text = ''
        // ==UserScript==
        // @name          Dark Reader (Unofficial)
        // @icon          https://darkreader.org/images/darkreader-icon-256x256.png
        // @namespace     DarkReader
        // @description	 Inverts the brightness of pages to reduce eye strain
        // @version       4.9.52
        // @author        https://github.com/darkreader/darkreader#contributors
        // @homepageURL   https://darkreader.org/ | https://github.com/darkreader/darkreader
        // @run-at        document-end
        // @grant         none
        // @exclude       https://git.dotya.ml*
        // @exclude       https://dotya.ml*
        // @exclude       https://status.dotya.ml*
        // @exclude       https://searxng.dotya.ml*
        // @exclude       https://grafana.dotya.ml*
        // @exclude       https://github.com*
        // @exclude       https://dnswatch.com*
        // @exclude       https://docs.immudb.io*
        // @exclude       https://woodpecker-ci.org*
        // @exclude       https://duckduckgo.com*
        // @exclude       https://www.redit.com*
        // @exclude       https://codeberg.org*
        // @include       http*
        // @require       https://cdn.jsdelivr.net/npm/darkreader/darkreader.min.js
        // @noframes
        // ==/UserScript==

        DarkReader.enable({
            brightness: 105,
            contrast: 105,
            sepia: 0
        });
        DarkReader.disable();
      '';
    };
  };

  xdg = (import ./nix/xdg.nix) {inherit pkgs config hostName;};
}
