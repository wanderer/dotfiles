# zmodload zsh/zprof

[ ! -d ~/.oh-my-zsh ] && sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"


 ZSH=$HOME/.oh-my-zsh
 ZSH_CACHE_DIR="$ZSH/cache"
 ZSH_COMPDUMP="~/.zcompdump"
 ZSH_DISABLE_COMPFIX=true

 fpath+=~/.zfunc

 [ -z "$IN_NIX_SHELL" ] && [ -f $HOME/.asdf/asdf.sh ] \
   && source $HOME/.asdf/asdf.sh \
   && fpath+=${ASDF_DIR/completions}

 # fpath=(
    # ~/.zfunc
    # $fpath
 # )

 eval "$(sheldon source)"

 # disable unneeded fast-syntax-highlighting modules.
 FAST_HIGHLIGHT[chroma-svn]=0
 FAST_HIGHLIGHT[chroma-cvs]=0
 FAST_HIGHLIGHT[chroma-aws]=0
 FAST_HIGHLIGHT[chroma-yard]=0
 FAST_HIGHLIGHT[chroma-cabal]=0
 FAST_HIGHLIGHT[chroma-exercism]=0

 test -f ~/.zsh/omz.zsh && source ~/.zsh/omz.zsh

 ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern)

 ZSH_AUTOSUGGEST_STRATEGY=(history completion)
 ZSH_AUTOSUGGEST_USE_ASYNC=

 ZSH_THEME_GIT_PROMPT_CACHE=" "


 test -f ~/.zsh/sources.zsh && source ~/.zsh/sources.zsh

# You may need to manually set your language environment
 LANG=en_GB.UTF-8
 http_proxy="http://localhost:8118"

 export EDITOR='vim'
 export VISUAL='vim'
 export SYSTEMD_EDITOR='vim --clean'
 export TERM='xterm-256color'
 # export TERM='xterm-kitty'
 export GTK_IM_MODULE=ibus
 export QT_IM_MODULE=ibus
 export XMODIFIERS=@im=ibus
# export ARCHFLAGS="-arch x86_64"
 export kee=${kee:-$(hostname -s)}
 SSH_KEY_PATH="$HOME/.ssh/$kee"
 # irrelevant when using exa.
 # eval $(TERM=xterm-kitty dircolors)
 # pretend we're dockerd for rootless podman.
 export DOCKER_HOST=unix://$XDG_RUNTIME_DIR/podman/podman.sock


 eval "$(starship init zsh)"

 # make home-manager not manage the shell configuration
 HMVARSFILE="$HOME/.nix-profile/etc/profile.d/hm-session-vars.sh"
 if [ -f "$HMVARSFILE" ]; then . "$HMVARSFILE"; fi

 if [ -e ~/.nix-profile/etc/profile.d/nix.sh ]; then . ~/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer...and kept by me
 eval "$(direnv hook zsh)"

 zstyle ':completion:*:*:*:*:descriptions' format '%F{green}-- %d --%f'
 zstyle ':completion:*:*:*:*:corrections' format '%F{yellow}!- %d (errors: %e) -!%f'
 zstyle ':completion:*:messages' format ' %F{purple} -- %d --%f'
 zstyle ':completion:*:warnings' format ' %F{red}-- no matches found --%f'
 # zstyle ':completion:*' group-name ''
 zstyle ':completion:*:*:-command-:*:*' group-order alias builtins functions commands
 # zstyle ':completion:*' file-list all
 zstyle ':completion:*' squeeze-slashes true
 zstyle ':completion:*:*:cp:*' file-sort modification
 # This way the completion script does not have to parse Bazel's options
 # repeatedly. The 'cache-path' directory in must be created manually.
 zstyle ':completion:*' use-cache on
 zstyle ':completion:*' cache-path $ZSH_CACHE_DIR

# zprof
