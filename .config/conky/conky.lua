conky.config = {
--background= false,
--xftfont= 'DejaVu Sans Mono:size=9',
font= 'DejaVu Sans Mono:style=regular:size=9.5',
--font= 'Ubuntu Mono:style=regular:size=10',
use_xft= true,
xftalpha= 0.7,
update_interval= 1,
update_interval_on_battery= 3.0,
total_run_times= 0,
own_window= true,
own_window_type= 'desktop',
own_window_transparent= true,
own_window_hints= 'undecorated,below,sticky,skip_taskbar,skip_pager',
--max_port_monitor_connections= 256,
double_buffer= true,
minimum_width= 256,
maximum_width= 256,
--short_units= false,
draw_shades= false,
draw_outline= false,
draw_borders= false,
draw_graph_borders= false,
default_color= 'white',
--default_shade_color= 'black',
--default_outline_color= 'black',
alignment= 'top_right',
gap_x= 13,
gap_y= 30,
no_buffers= true,
uppercase= false,
--diskio_avg_samples= 3,
cpu_avg_samples= 2,
net_avg_samples= 2,
override_utf8_locale= true,
out_to_console= false,
out_to_stderr= false,
out_to_x= true,
};

--user = os.execute("whoami")

conky.text = [[
#
${alignr}${no_update kernel: ${kernel}}
${alignr}load: $loadavg
${alignr}uptime: ${uptime}
${if_match ${battery_percent} <= 15}\
${color ff1a21}${alignr}${blink battery: ${battery_short} $battery_time}
$else\
${color white}${alignr}battery: ${battery_short}$battery_time
$endif${color white}\
${alignr}core0: ${texeci 10 bc <<< "`cat /sys/class/thermal/thermal_zone5/temp` / 1000"}°C  \
core1: ${texeci 10 bc <<< "`cat /sys/class/thermal/thermal_zone6/temp` / 1000"}°C\

${alignr}${cpugraph cpu1 20,110} ${cpugraph cpu2 20,110 -t 9966ff 6699ff}
${goto 65}cpu0: ${cpu cpu1}% \
${goto 185}cpu1: ${cpu cpu2}%
${alignr}${cpugraph cpu3 20,110} ${cpugraph cpu4 20,110 -t 9966ff 6699ff}
${goto 65}cpu2: ${cpu cpu3}% \
${goto 185}cpu3: ${cpu cpu4}%

${alignr}ram ${alignr}$memperc% = $mem/$memmax
${alignr}swap ${alignr}$swapperc% = $swap/$swapmax
#
${alignr}/ ${fs_used_perc /}% = ${fs_used /}/${fs_size /}
${alignr}/home ${fs_used_perc /home}% = ${fs_used /home}/${fs_size /home}
#
${if_mounted /run/media/vis/data}${alignr}data ${fs_used_perc /run/media/vis/data}% = \
${fs_used /run/media/vis/data}/\
${fs_size /run/media/vis/data}
${endif}
#
#top cpu ${goto 130} pid ${goto 180} cpu% \
#${goto 260}top mem${alignr}pid   mem%
#${color0}${top name 1}${goto 125}${top pid 1}${goto 175}${top cpu 1}  \
#${color0}${goto 260}${top_mem name 1}${alignr}${top_mem pid 1}${top_mem mem 1}
#${color0}${top name 2}${goto 125}${top pid 2}${goto 175}${top cpu 2}  \
#${color0}${goto 260}${top_mem name 2}${alignr}${top_mem pid 2}${top_mem mem 2}
#${color0}${top name 3}${goto 125}${top pid 3}${goto 175}${top cpu 3}  \
#${color0}${goto 260}${top_mem name 3}${alignr}${top_mem pid 3}${top_mem mem 3}
#${color0}${top name 4}${goto 125}${top pid 4}${goto 175}${top cpu 4}  \
#${color0}${goto 260}${top_mem name 4}${alignr}${top_mem pid 4}${top_mem mem 4}
#${color0}${top name 5}${goto 125}${top pid 5}${goto 175}${top cpu 5}  \
#${color0}${goto 260}${top_mem name 5}${alignr}${top_mem pid 5}${top_mem mem 5}
#
${if_up wlp2s0}\
${color 009999}${alignr}wlp2s0
${alignr}${color 6699ff}essid: ${color 9966ff}${wireless_essid wlp2s0}
${alignr}${color 6699ff}Q: ${color white}${wireless_link_qual_perc wlp2s0}% \
${color 6699ff}mode: ${color 9966ff}${wireless_mode wlp2s0}
${alignr}${color 6699ff}channel: ${color 9966ff}${wireless_channel wlp2s0} \
${color 6699ff}freq: ${color0}${wireless_freq wlp2s0}
${alignr}${color 6699ff}gw: ${color 00cc88}${texeci 30 route -n | awk '/UG/ && /wlp2s0/ { print $2 }'}
${color 6699ff}${alignr}ap mac: ${color 9966ff}${texeci 30 arp | awk '/gateway/ && /wlp2s0/ { print $3 }'}
${alignr}${color 6699ff}ip: ${color 00cc88}${addr wlp2s0}
#get the external ip using opendns :(
${alignr}${color 6699ff}pub ip: ${color 00cc88}${texeci 30 dig +short myip.opendns.com @208.67.222.222}
${color 6699ff}${alignr}mac: ${color 9966ff}${texeci 300 ifconfig wlp2s0 | awk '/ether/ && /txque/ { print $2 }'}
#
#${color white}total ${totaldown wlp2s0}\
#${goto 135}down ${downspeed wlp2s0}\
#${goto 235}↓↑${goto 270}up ${upspeed wlp2s0}\
#${alignr}total ${totalup wlp2s0}
#${downspeedgraph wlp2s0 25,225 9966ff 6699ff -l} · \
#${alignr}${upspeedgraph wlp2s0 25,225 9966ff 6699ff -l}
${alignr}↑ ${color 6699ff}up ${color 9966ff}${upspeed wlp2s0} \
${color 6699ff}total ${color 9966ff}${totalup wlp2s0}
${alignr}${upspeedgraph wlp2s0 25,225 6699ff 9966ff -l}
${alignr}↓ ${color 6699ff}down ${color 9966ff}${downspeed wlp2s0} \
${color 6699ff}total ${color 9966ff}${totaldown wlp2s0}
${alignr}${downspeedgraph wlp2s0 25,225 6699ff 9966ff -l}
${endif}\
#
${if_up enp0s31f6}\
${alignr}${color 009999}enp0s31f6
${alignr}${color 6699ff}dns: ${color 00cc88}${texeci 30 cat /etc/resolv.conf | grep nameserver | sed '{s/ *nameserver *//;s///}' | head -1}
${alignr}${color 6699ff}gw: ${color 00cc88}${texeci 30 route -n | awk '/UG/ && /enp0s31f6/ { print $2 }' | head -1}
${alignr}${color 6699ff}gw mac: ${color 9966ff}${texeci 30 arp | awk '/gateway/ && /enp0s31f6/ { print $3 }'}
${alignr}${color 6699ff}ip:${color 00cc88} ${addr enp0s31f6}
#get the external ip using opendns
${alignr}${color 6699ff}pub ip: ${color 00cc88}${texeci 30 dig +short myip.opendns.com @208.67.222.222}
${color 6699ff}${alignr}mac: ${color 9966ff}${texeci 300 ifconfig enp0s31f6 | awk '/ether/ && /txque/ { print $2 }'}
#
${alignr}↑ ${color 6699ff}up ${color 9966ff}${upspeed enp0s31f6} \
${color 6699ff}total ${color 9966ff}${totalup enp0s31f6}
${alignr}${upspeedgraph enp0s31f6 25,225 6699ff 9966ff -l}
${alignr}↓ ${color 6699ff}down ${color 9966ff}${downspeed enp0s31f6} \
${color 6699ff}total ${color 9966ff}${totaldown enp0s31f6}
${alignr}${downspeedgraph enp0s31f6 25,225 6699ff 9966ff -l}
${endif}\
${if_gw}\
${goto 55}inbound: ${tcp_portmon 1 32767 count} ${alignr}outbound: ${tcp_portmon 32768 61000 count}
${goto 50}${color}rhost ${alignr}all: ${tcp_portmon 1 65535 count}     rport
${goto 30}${tcp_portmon 1 65535 rip  0}${alignr 1}${tcp_portmon 1 65535 rport  0}
${goto 30}${tcp_portmon 1 65535 rip  1}${alignr 1}${tcp_portmon 1 65535 rport  1}
${goto 30}${tcp_portmon 1 65535 rip  2}${alignr 1}${tcp_portmon 1 65535 rport  2}
${goto 30}${tcp_portmon 1 65535 rip  3}${alignr 1}${tcp_portmon 1 65535 rport  3}
${goto 30}${tcp_portmon 1 65535 rip  4}${alignr 1}${tcp_portmon 1 65535 rport  4}
${goto 30}${tcp_portmon 1 65535 rip  5}${alignr 1}${tcp_portmon 1 65535 rport  5}
${goto 30}${tcp_portmon 1 65535 rip  6}${alignr 1}${tcp_portmon 1 65535 rport  6}
${goto 30}${tcp_portmon 1 65535 rip  7}${alignr 1}${tcp_portmon 1 65535 rport  7}
${goto 30}${tcp_portmon 1 65535 rip  8}${alignr 1}${tcp_portmon 1 65535 rport  8}
${goto 30}${tcp_portmon 1 65535 rip  9}${alignr 1}${tcp_portmon 1 65535 rport  9}
${endif}
]]
