#!/bin/bash

# This script creates a screenshot of the current workspace, pixelates (and blurs) it
# deletes the screenshot, then it activates i3lock screenlocker and suspends the pc.
# scrot/convert part is adapted from https://faq.i3wm.org/question/83/how-to-run-i3lock-after-computer-inactivity/

# cappuccino is inspired by but not based on the caffeine application
# Inhibit screen locker use -f switch to force screen locker
#if [ "$1" != "-f" -a -f "/home/$USER/.config/openbox/cappuccino" ]; then
#    exit
#fi

pkill -9 light-locker & # make sure this one doesn't interfere as it loves to

wdir=$HOME/.tmp/lckr
screen1=$wdir/l1.png
lwaucj=$wdir/nuvdsp.png
mkdir -p $wdir && chmod 1700 $wdir

scrot $screen1 # take a screenshot

# Pixelate (and blur) image using convert (could use mogrify but it's slower)
#convert $screen1 -scale 40% -scale 250% -blur 10x10 $screen1 && rm $screen1 - ?
#convert $screen1 -scale 10% -scale 1000% -blur 10x10 $lwaucj && rm $screen1
convert $screen1 -scale 10% -scale 1000% $lwaucj && rm $screen1

#xset dpms force suspend

# Lock screen using i3lock showing number of failed attempts, not forking,
# ignoring empty password and without unlock indicator (using it now though)
i3lock -f -n -e -i "$lwaucj" &
#i3lock -f -n -e -u -i /home/fedora/Pictures/fedora/n/fork.png &
#i3lock -f -n -e -u -i "/usr/share/backgrounds/Fork.png" &
sleep 0.1337
systemctl suspend

sleep 3.1337
xset dpms 5 5 5
xset dpms force on

# running in background until the next login
state=`pgrep i3lock > /dev/null; echo $?`
while : ; do
  if [ $state -ne 0 ]; then 
    break
  else
    sleep 3.1337
  fi
  break
done
    
# Set display to not turn off automatically again
xset dpms 0 0 0
xset dpms force on
rm -Rf $HOME/.tmp
#light-locker & - not using lightdm now
notify-send 'Welcome back!'
