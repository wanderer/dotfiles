#!/bin/bash

# This script creates a screenshot of the current workspace, pixelates and blurs it
# deletes the screenshot, then it activates i3lock screenlocker and switches off the display.
# scrot/convert part is adapted from https://faq.i3wm.org/question/83/how-to-run-i3lock-after-computer-inactivity/

# allegedly works without closing the current session with lightdm
#dm-tool switch-to-guest; dm-tool switch-to-greeter

# Inhibit screen locker when media players are running
# Cheap method, add missing players to regex
isrunning=1
player=`ps -u $USER | grep -Ec "(rhythmbox|totem|mpv|vlc|*mplayer)"`
notify-send "`echo $1`"
if [ $1 == "-f" ]; then
  break
else
  if [ "$player" -ge "$isrunning"  ]; then
    exit 1
  fi
fi

pkill -9 light-locker # we don't want interference

revert() {
  xset dpms 0 0 0
  xset dpms force on
  rm -Rf $HOME/.tmp
  #light-locker & - not using lightdm currently
}
xset dpms 5 5 5 

wdir=$HOME/.tmp/lckr
screen1=$wdir/l1.png
lwaucj=$wdir/nuvdsp.png
mkdir -p $wdir && chmod 1700 $wdir

scrot $screen1 # take a screenshot

#convert $screen1 -scale 40% -scale 250% -blur 10x10 $lwaucj && rm $screen1
#convert $screen1 -scale 10% -scale 1000% -blur 10x10 $lwaucj && rm $screen1
convert $screen1 -scale 10% -scale 1000% $lwaucj && rm $screen1

# xset dpms force off (or suspend) && sleep shortly

# Lock screen using i3lock showing number of failed attempts, not forking,
# ignoring empty password and without unlock indicator (-u)
i3lock -f -n -e -i "$lwaucj" || i3lock -f -e -n -c 141414 # just dark-grey
trap revert exit
# EXIT HUP INT TERM - 0 1 3 15

revert
notify-send 'Welcome back!'
