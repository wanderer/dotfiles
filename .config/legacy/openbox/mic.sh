#!/bin/bash

# this is 'mic mute toggler' for Openbox on a t420 using ALSA

export INPUT_DEVICE="'Capture'"
amixer sset $INPUT_DEVICE,0 toggle
