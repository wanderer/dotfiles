#!/bin/bash
# this is an openbox-session autostart helper little script

function one() {
for i in {1..3..1}
  do
    pkill -15 gnome-software 2> /dev/null
    pkill -15 blue
    pkill -15 switcheroo
    pkill -9 goa
    pkill -15 volumeicon
    pkill -15 cinnamon-screen 2> /dev/null
    pkill -15 boltd 2> /dev/null # no thunderbolt on my t420 :)

    #echo "[i] autokill no. $i" 2> /dev/null
    notify-send '[i] autokill no. '$i 2> /dev/null
    sleep 33
  done
}

sleep 7

one &

sleep 5

notify-send -t 10000 'tor' "`netstat -ant | grep 9050`"
sleep 5
notify-send -t 10000 'privoxy' "`netstat -ant | grep 8118`"
sleep 5
notify-send -t 10000 'external ip' "`curl ifconfig.me`"
sleep 5
notify-send -t 10000 'ssh connections' "`netstat -ant | grep 22`"
