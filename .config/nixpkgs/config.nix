{
  pkgs = import <personal> { };

  # https://nixos.org/manual/nixpkgs/stable/#sec-declarative-package-management
  # https://github.com/g-w1/nix-configs/blob/main/home.nix
  packageOverrides = pkgs:
    with pkgs; {
      myPackages = pkgs.buildEnv {
        name = "my-packages";
        paths = [
          aerc
          b3sum
          cpplint

          cachix
          hydra-check
          nixops
          nix-update
          nix-diff
          niff
          nox
          nix-du
          nixfmt
          statix

          elvish
          silver-searcher
          starship
          ttyper

          nerdfonts
        ];
        pathsToLink = [ "/share" "/bin" "/etc" ];
      };
    };

    nix.distributedBuilds = true;
    # useful when the builder has a faster internet connection than yours
    nix.extraOptions = ''
		builders-use-substitutes = true
	'';
}
