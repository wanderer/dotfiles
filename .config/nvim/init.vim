" not necessary, as vim will turn this on by default, when .vimrc is found
" set nocompatible

set encoding=utf-8            " important for powerline besides others
scriptencoding=utf8

filetype off                  "required

augroup install_vim_plug
  autocmd!
  if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
    silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
      \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  endif
augroup END

augroup plug_auto_install
  autocmd!
  " Run PlugInstall if there are missing plugins
  autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
    \| PlugInstall --sync | source $MYVIMRC
    \| endif
augroup END


call plug#begin()
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'ryanoasis/vim-devicons'

Plug 'dracula/vim'
Plug 'axvr/photon.vim'

Plug 'ncm2/ncm2'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

Plug 'ctrlpvim/ctrlp.vim'
Plug 'mhinz/vim-startify'

Plug 'preservim/nerdcommenter'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'

Plug 'williamboman/mason.nvim'
Plug 'williamboman/mason-lspconfig.nvim'
Plug 'neovim/nvim-lspconfig'
""  Plug 'kabouzeid/nvim-lspinstall'

"Plug 'zchee/nvim-go', { 'do': 'make'}
Plug 'fatih/vim-go', { 'for': ['go', 'gomod', 'gohtmltmpl', 'gotexttmpl'], 'do': ':GoInstallBinaries' }
Plug 'themaxmarchuk/tailwindcss-colors.nvim'
Plug 'brenoprata10/nvim-highlight-colors'

Plug 'majutsushi/tagbar'
Plug 'NoahTheDuke/vim-just', { 'for': ['just'] }
Plug 'fladson/vim-kitty', { 'for': ['kitty'] }
call plug#end()


" airline config
" turn on vimscript 9 support
let g:airline_experimental = 1

let g:airline_powerline_fonts = 1
let g:airline_highlighting_cache = 1
" violet, wombat, deus
let g:airline_theme = 'minimalist'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
" show tab number instead of number of splits
let g:airline#extensions#tabline#tab_nr_type = 1

let g:airline_mode_map = {
      \ '__'     : '-',
      \ 'c'      : 'C',
      \ 'i'      : 'I',
      \ 'ic'     : 'I-C',
      \ 'ix'     : 'I-X',
      \ 'n'      : 'N',
      \ 'multi'  : 'M',
      \ 'ni'     : 'N',
      \ 'no'     : 'N',
      \ 'R'      : 'R',
      \ 'Rv'     : 'R',
      \ 's'      : 'S',
      \ 'S'      : 'S',
      \ ''     : 'S',
      \ 't'      : 'T',
      \ 'v'      : 'V',
      \ 'V'      : 'V-L',
      \ ''     : 'V-B',
      \ }

" skip displaying fileformat output if the output matches a configured string.
let g:airline#parts#ffenc#skip_expected_string='utf-8[unix]'

" certain number of spaces are allowed after tabs, but not in between.
" this algorithm works well for /** */ style comments in a tab-indented file.
let g:airline#extensions#whitespace#mixed_indent_algo = 1

" turn off the warning section entirely.
let g:airline_section_warning = ''

let g:airline_skip_empty_sections = 1

" separator symbols
let g:airline_left_sep = ''
let g:airline_right_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_alt_sep = ''

if !exists('g:airline_symbols')
	let g:airline_symbols = {}
endif

let g:airline_symbols.branch = ''
let g:airline_symbols.colnr = '℅:'
let g:airline_symbols.readonly = ''
let g:airline_symbols.nx='∄'

" ale integration
let g:airline#extensions#ale#enabled = 1
" nrrwrgn integration
let g:airline#extensions#nrrwrgn#enabled = 1
" vcs integration
let g:airline#extensions#branch#enabled = 0
" show summary of changed hunks (gitgutter and/or vim-signify)
let g:airline#extensions#hunks#enabled = 1
" tabline integration
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#ctrlp#enabled = 1
let g:airline#extensions#taboo#enabled = 0
let g:airline#extensions#battery#enabled = 0
let g:airline#extensions#bookmark#enabled = 0
let g:airline#extensions#flog#enabled = 0
let g:airline#extensions#bufferline#enabled = 0
let g:airline#extensions#capslock#enabled = 0
let g:airline#extensions#coc#enabled = 0
" syntastic integration
let g:airline#extensions#syntastic#enabled = 0
" vim-csv
let g:airline#extensions#csv#enabled = 0
let g:airline#extensions#ctrlspace#enabled = 0
" Built-in extension to displays cursor in different colors depending on the
" current mode (only works in terminals iTerm, AppleTerm and xterm)
" * this fixes the issue of airline changing terminal cursor colour in kitty+fedora35/fedora36
let g:airline#extensions#cursormode#enabled = 0


if has('patch-8.1.1880')
  " set completeopt=longest,menuone,popuphidden
  set completeopt=longest,menuone,popup
  " Highlight the completion documentation popup background/foreground the same as
  " the completion menu itself, for better readability with highlighted
  " documentation.
  set completepopup=highlight:Pmenu,border:off
else
  set completeopt=longest,menuone,preview
  " Set desired preview window height for viewing documentation.
  set previewheight=5
endif

" filler lines to keep text position; start diff with vertical splits; don't
" ignore case and whitespace changes
set diffopt=filler,vertical

" set scrolloff=3               " scroll offset, min lines above/below cursor
set scrolloff=5               " scroll offset, min lines above/below cursor
"set scrolljump=5             " jump 5 lines when running out of the screen - NO
set sidescroll=10             " minimum columns to scroll horizontally
" Make <,> match pairs, too.
" https://github.com/EHartC/dot-vimrc/blob/master/vim%20config/styling.vim
set matchpairs+=<:>
set matchtime=2               " how many tenths of a second to blink when matching brackets
set noshowmode                " show editing mode in status (-- INSERT --)
set ruler                     " show cursor position

" Tell vim to remember certain things when we exit
"  '10  :  marks will be remembered for up to 10 previously edited files
"  "100 :  will save up to 100 lines for each register
"  :20  :  up to 20 lines of command-line history will be remembered
"  %    :  saves and restores the buffer list
set viminfo='100,"100,:20,%           " '=marks for x files, "=registers for x files

" set nofoldenable
set foldmethod=manual         " manual folding

set fileencoding=utf-8        " set encoding for newly saved files
set cmdheight=2               " lines under {status,air}line

" colo dracula
colo photon
set autochdir
set number relativenumber
set smartcase
set splitbelow splitright
set lazyredraw
set ttyfast
set listchars=trail:•,tab:>-,nbsp:☣,eol:¬,extends:❯,precedes:❮
set nolist                    " show/hide tabs and EOL chars (hidden characters)
" don't indent wrapped lines
set nobreakindent

set confirm                   " ask confirmation like save before quit.
set wildmenu                  " Tab completion menu when using command mode
" https://bitbucket.org/sjl/dotfiles/src/tip/vim/vimrc#cl-153
set wildignore+=.hg,.git,.svn                    " Version control
set wildignore+=*.aux,*.out,*.toc                " LaTeX intermediate files
set wildignore+=*.jpg,*.bmp,*.gif,*.png,*.jpeg   " binary images
set wildignore+=*.o,*.obj,*.exe,*.dll,*.manifest " compiled object files
set wildignore+=*.dwo                            " DWARF debug object files
set wildignore+=*.spl                            " compiled spelling word lists
set wildignore+=*.pyc                            " Python byte code
set wildignore+=node_modules/**
set wildmode=longest:longest,full

set colorcolumn=80
filetype plugin indent on
set hlsearch incsearch

augroup filetypes
	autocmd!
	au BufRead,BufNewFile *.tmpl setlocal filetype=gohtmltmpl
	au FileType gohtmltmpl setlocal ff=unix ts=2 shiftwidth=2 sts=2 noexpandtab
  au FileType nix setl
      \ fileformat=unix autoindent omnifunc=lsp#complete
  au BufNewFile,BufRead,BufEnter *.js
      \ setlocal shiftwidth=2 tabstop=2 softtabstop=2 backspace=2 expandtab autoindent
      \ fileformat=unix filetype=javascript

  au BufNewFile,BufRead,BufEnter *.md
      \ setlocal textwidth=79 foldmethod=manual filetype=markdown
      \ fileformat=unix expandtab

  au FileType swayconfig setl textwidth=99
      \ tabstop=4 shiftwidth=4 softtabstop=4 noexpandtab autoindent ft=swayconfig

  au FileType rust setl
      \ tabstop=8 shiftwidth=8 softtabstop=8 noexpandtab autoindent ff=unix
augroup END


" --------
" plugin configurations
" --------

let g:gitgutter_terminal_reports_focus=0
let g:gitgutter_async = 1
let g:gitgutter_sign_column_always = 1
let g:gitgutter_sign_allow_clobber=0
let g:gitgutter_set_sign_backgrounds = 0
let g:gitgutter_preview_win_floating = 1
set foldtext=gitgutter#fold#foldtext()
nmap ghs <Plug>(GitGutterStageHunk)
nmap ghu <Plug>(GitGutterUndoHunk)
nnoremap <leader>d :GitGutterLineHighlightsToggle<cr>



" nerdcommenter
noremap <silent><c-/> :call nerdcommenter#Comment(0,"toggle")<cr>
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1
" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1
" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1
" Add your own custom formats or override the defaults
let g:NERDCustomDelimiters = {
            \ 'vim': { 'left': '"','right': '' },
            \ 'c': {'left': '/**','right': '*/' },
            \ 'dhall': {'left': '-- | ', 'right': '' },
            \ 'zsh': { 'left': '#','right': ''},
            \ 'sysctl': {'left': '#', 'right': ''},
            \ 'systemd': {'left': ';', 'right': ''},
            \}
" let g:NERDTreeGitStatusWithFlags = 1


" netrw
let g:netrw_banner       = 0
let g:netrw_keepdir      = 0
" prefer tree-style listing
let g:netrw_liststyle    = 3
" let g:netrw_liststyle    = 1
let g:netrw_sort_options = 'i'
" open files in a new horizontal split
let g:netrw_browse_split = 1


" ctrlp
" Setup some default ignores
let g:ctrlp_custom_ignore = {
	\ 'dir':'\v[\/](\.(git\/(branches|hooks|info|logs|modules|objects|rebase-merge|refs)|hg|svn)|\_site)$',
	\ 'file':'\v\.(exe|so|dll|class|png|jpg|jpeg)|(\.(git\/(description|index|packed-refs|COMMIT_EDITMSG|HEAD|FETCH_HEAD|ORIG_HEAD|REBASE_HEAD)))$',
	\}

" Use the nearest .git directory as the cwd
" This makes a lot of sense if you are working on a project that is in version
" control. It also supports works with .svn, .hg, .bzr.
" let g:ctrlp_working_path_mode = 'r'
let g:ctrlp_working_path_mode = 'raw'
let g:ctrlp_cache_dir = $HOME . '/.cache/ctrlp'

" The Silver Searcher
if executable('ag')
  " use ag instead of grep.
  set grepprg=ag\ --nogroup\ --nocolor

  " use ag in CtrlP
  let g:ctrlp_user_command = 'ag %s -l --nocolor --hidden -g ""'
  " ag is fast enough that CtrlP doesn't need to cache
  let g:ctrlp_use_caching = 0
endif

nnoremap <c-p> :CtrlP<cr><cr>
nnoremap <leader>s <Plug>(ctrlp)

let g:ctrlp_show_hidden = 1
" Use a leader instead of the actual named binding
nmap <leader>p <Plug>(ctrlp)

" ctrlp buffers
" Easy bindings for its various modes
nmap <leader>bb :CtrlPBuffer<cr>
nmap <leader>bm :CtrlPMixed<cr>
nmap <leader>bs :CtrlPMRU<cr>

" buffers
nmap <leader>T :enew<cr>
nmap <leader>bq :bp <BAR> bd #<cr>
nmap <leader>gd :bd<cr>
map gn :bn<cr>
map gp :bp<cr>

command! W execute 'w' | edit!
command! WW execute 'silent w !sudo tee % >/dev/null' | edit!

" always show full path on <c-g>
nnoremap <c-g> 1<c-g>
" open a terminal in $PWD
nnoremap <silent> <leader>tt :terminal<cr>
" terminal normal mode
tnoremap <F1> <C-W>N
" toggle showing whitespace
noremap <F5> :set list!<cr>
nnoremap <F8> :TagbarToggle<cr>
" [bro]wse [ol]d files
nnoremap <leader>r :bro ol<cr>

" tab switching
nnoremap <silent><f6> gT
nnoremap <silent><f7> gt

" resize windows
map - <C-W>-
map + <C-W>+

" vmap to stay in Visual Mode after shifting > and <
vmap < <gv
vmap > >gv


"------------------------------------
" vim-go.vim
"------------------------------------
" use gopls
let g:go_def_mode='gopls'
let g:go_info_mode='gopls'
" Status line types/signatures.
let g:go_auto_type_info = 1

let g:go_highlight_types = 1
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_structs = 1
let g:go_highlight_interfaces = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1
let g:go_highlight_extra_types = 1
" if sluggish
" let g:go_highlight_structs = 0
" let g:go_highlight_interfaces = 0
" let g:go_highlight_operators = 0
" let g:go_list_type = 'quickfix'
let g:go_list_type = ''
" let g:go_version_warning = 0
let g:go_fmt_fail_silently = 0
" auto formatting and importing
let g:go_fmt_command = 'gopls'
let g:go_fmt_autosave = 1
" go install mvdan.cc/gofumpt@latest
let g:go_gopls_gofumpt = 1
" let g:go_gopls_options = ['-remote=auto']
let g:go_gopls_options = ['-remote=unix;/tmp/.gopls-daemon.sock']
" let lsp handle connecting to gopls
let g:go_gopls_enabled = 1
" let g:go_doc_popup_window = 0
let g:go_doc_popup_window = 1
let g:go_fillstruct_mode = 'gopls'
let g:go_term_reuse = 1
let g:go_term_enabled = 1
let g:go_doc_balloon = 1


augroup vimgo
    au!
    au FileType go nmap <leader>gb <Plug>(go-build)
    au FileType go nmap <leader>b <Plug>(go-build)
    au FileType go nmap <leader>gr <Plug>(go-run)
    au FileType go nmap <leader>gt <Plug>(go-test)
    au FileType go nmap <leader>gs <Plug>(go-implements)
    au FileType go nmap <leader>gi <Plug>(go-info)
    au FileType go nmap <leader>ge <Plug>(go-rename)
    au FileType go nmap <leader>gd <Plug>(go-doc)
    " Navigation commands
    au FileType go nmap <leader>ds <Plug>(go-def-split)
    au FileType go nmap <leader>dv <Plug>(go-def-vertical)
    " Alternate commands
    au FileType go nmap <leader>hae <Plug>(go-alternate-edit)
    au FileType go nmap <leader>vae <Plug>(go-alternate-vertical)
    " Use new vim 8.2 popup windows for Go Doc
augroup END

" go_tags
"------------------------------------
exec 'source ' . escape(expand('~/.vim'),' ') . '/' . 'gotags.vimrc.vim'


" when quitting a file, save the cursor position
augroup save_cursor_position
  if &filetype !~# 'commit\|rebase'
    autocmd!
    " also automatically open any folds the cursor might land on.
    autocmd BufReadPost * silent! normal! g`" zv
  endif
augroup END


set hidden
set conceallevel=0
set mouse=a
set nogdefault

let g:vim_json_conceal = 0
let g:markdown_syntax_conceal = 0

set timeoutlen=200
set notimeout ttimeout ttimeoutlen=10
set undofile
set undodir=~/.vim/nvimundo//
set undolevels=2000
set undoreload=10001
if !isdirectory(expand(&undodir))
  call mkdir(expand(&undodir), 'p')
endif

set nobackup
set nowritebackup
set directory=~/.vim/swp//
if !isdirectory(expand(&directory))
  call mkdir(expand(&directory), 'p')
endif

set title
set showcmd
set showmatch
set updatetime=250
set noscrollbind
set noerrorbells
set laststatus=2                " show status bar
set report=1                    " Always report number of lines changed by ex commands
if (has('termguicolors'))
  set termguicolors
else
  set t_Co=256
endif
set guifont=Fira\ Code\ weight=450\ 12

let g:netrw_beval = 1
set t_vb=

" cursorline optimizations
set cursorlineopt=number
" 0	automatic selection
" 1	old engine
" 2	NFA engine
" set regexpengine=1
set regexpengine=0
" set redrawtime=500
" set synmaxcol=128
syntax sync minlines=256

" use +/clipboard as default register
set clipboard=unnamed,unnamedplus

" but since it's needed for direnv, we have to enable it.
set exrc
set secure

" https://sw.kovidgoyal.net/kitty/faq/#using-a-color-theme-with-a-background-color-does-not-work-well-in-vim
" work around vim's background colour erase:
"
" vim hardcodes background color erase even if the terminfo file does
" not contain bce (not to mention that libvte based terminals
" incorrectly contain bce in their terminfo files). This causes
" incorrect background rendering when using a color theme with a
" background color.
let &t_ut=''

" compensate for lack of theme italics
for s in ['Comment', 'markdownItalic', 'Boolean']
  execute 'hi ' s ' cterm=italic'
endfor
hi CursorLineNR cterm=bold


lua <<EOF
require('nvim-highlight-colors').setup {enable_tailwind = true}

require("mason").setup({
    ui = {
        icons = {
            package_installed = "✓",
            package_pending = "➜",
            package_uninstalled = "✗"
        }
    }
})
require("mason-lspconfig").setup()

local lspconfig = require('lspconfig')
local util = require "lspconfig/util"

lspconfig.rnix.setup {}
lspconfig.gotests.setup {}
lspconfig.bashls.setup {}
lspconfig.djlint.setup {}
lspconfig.ansiblels.setup {}
lspconfig.terraformls.setup {}
lspconfig.lua_ls.setup {}
lspconfig.dhall_lsp_server.setup {}
lspconfig.hadolint.setup {}
lspconfig.pylsp.setup {}
lspconfig.shfmt.setup {}
lspconfig.vint.setup {}
lspconfig.yamllint.setup {}
lspconfig.zls.setup {}

lspconfig.golangci_lint.setup {}
lspconfig.golangci_lint_ls.setup {}
-- https://go.googlesource.com/tools/+/refs/heads/master/gopls/doc/vim.md
lspconfig.gopls.setup{
  cmd = {"gopls", "-remote=unix;/tmp/.gopls-daemon.sock"},
	filetypes = {"go", "gomod", "gowork", "gotmpl", "gohtmltmpl"},
  root_dir = util.root_pattern("go.work", "go.mod", ".git"),
  settings = {
    gopls = {
      analyses = {
        unusedparams = true,
      },
      staticcheck = true,
    },
  },
}
lspconfig.vimls.setup{}
lspconfig.tailwindcss.setup{
	filetypes = { "aspnetcorerazor", "astro", "astro-markdown", "blade", "clojure", "django-html", "htmldjango", "edge", "eelixir", "elixir", "ejs", "erb", "eruby", "gohtml", "gohtmltmpl", "gotmpl", "haml", "handlebars", "hbs", "html", "html-eex", "heex", "jade", "leaf", "liquid", "markdown", "mdx", "mustache", "njk", "nunjucks", "php", "razor", "slim", "twig", "css", "less", "postcss", "sass", "scss", "stylus", "sugarss", "javascript", "javascriptreact", "reason", "rescript", "typescript", "typescriptreact", "vue", "svelte" }
}
lspconfig.html.setup{}


vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next)

-- Use LspAttach autocommand to only map the following keys
-- after the language server attaches to the current buffer
vim.api.nvim_create_autocmd('LspAttach', {
  group = vim.api.nvim_create_augroup('UserLspConfig', {}),
  callback = function(ev)
    -- Enable completion triggered by <c-x><c-o>
    vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

    local complopts = {silent = true, noremap = true, expr = true, replace_keycodes = false}
    vim.keymap.set("i", "<TAB>", [[pumvisible() ? "\<c-n>" : "\<TAB>"]], complopts)
    vim.keymap.set("i", "<S-TAB>", [[pumvisible() ? "\<c-p>" : "\<C-h>"]], complopts)
    vim.keymap.set("i", "<cr>", [[pumvisible() ? "\<c-y>" : "\<cr>"]], complopts)

    -- Buffer local mappings.
    -- See `:help vim.lsp.*` for documentation on any of the below functions
    local opts = { buffer = ev.buf }
    vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
    vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
    vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
    vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
    vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)
    vim.keymap.set('n', '<leader>wa', vim.lsp.buf.add_workspace_folder, opts)
    vim.keymap.set('n', '<leader>wr', vim.lsp.buf.remove_workspace_folder, opts)
    vim.keymap.set('n', '<leader>wl', function()
      print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end, opts)
    vim.keymap.set('n', '<leader>D', vim.lsp.buf.type_definition, opts)
    vim.keymap.set('n', '<leader>rn', vim.lsp.buf.rename, opts)
    vim.keymap.set({ 'n', 'v' }, '<leader>ca', vim.lsp.buf.code_action, opts)
    vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
    vim.keymap.set('n', '<leader>f', function()
      vim.lsp.buf.format { async = true }
    end, opts)
  end,
})


-- https://dev.to/vonheikemen/neovim-using-vim-plug-in-lua-3oom

EOF


" vim: ft=vim ff=unix expandtab ts=2 sw=2
