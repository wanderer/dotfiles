#!/bin/sh

mount="/"
warning=20
critical=10

/usr/bin/env df -h -P -l "$mount" | /usr/bin/env awk -v warning=$warning -v critical=$critical '
/\/.*/ {
  text=$4
  use=$5
  exit 0
}
END {
  class=""
  gsub(/%$/,"",use)
  if ((100 - use) < critical) {
    class="critical"
  } else if ((100 - use) < warning) {
    class="warning"
  }
  print "{\"text\":\""text"\", \"percentage\":"use", \"class\":\""class"\"}"
}
'
