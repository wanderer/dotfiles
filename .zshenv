# ~/.zshenv

export GOPATH="$HOME/go"
export GOBIN="$GOPATH/bin"

export ANDROID_SDK_ROOT="$HOME/utils/Android/Sdk"
export ANDROID_SDK_PATH=$ANDROID_SDK_ROOT
export ANDROID_NDK_PATH=$ANDROID_SDK_ROOT/ndk-bundle
export ANDROID_NDK_HOME=$ANDROID_NDK_PATH
export AndroidSdkPath=$ANDROID_SDK_ROOT
export AndroidNdkPath=$ANDROID_NDK_PATH

export CCACHE_NODISABLE="true"

# https://github.com/nix-community/home-manager/issues/4019
export SKIP_SANITY_CHECKS=1

# https://github.com/NixOS/nix/issues/2033
# https://discourse.nixos.org/t/where-is-nix-path-supposed-to-be-set/16434/8
export NIX_PATH=$HOME/.nix-defexpr/channels:/nix/var/nix/profiles/per-user/vis/channels${NIX_PATH:+:$NIX_PATH}

[ -z "$IN_NIX_SHELL" ] && export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin"


add_to_path() {
	local p=$1
	if [[ ! "$PATH" == *"$p"* ]]; then
		export PATH="$p:$PATH"
	fi
}

local __paths=(
	"/usr/lib64/ccache"
	"$ANDROID_SDK_ROOT/platform-tools"
	"$ANDROID_SDK_ROOT/cmdline-tools/latest/bin"
	"$ANDROID_SDK_ROOT/tools"
	"$ANDROID_SDK_ROOT/tools/bin"
	"$HOME/.local/share/JetBrains/Toolbox/bin"
	"$HOME/.npm-packages/bin"
	"/var/lib/flatpak/exports/bin"
	"$HOME/.local/share/flatpak/exports/bin"
	"/opt/metasploit-framework/bin"
	"$HOME/.cargo/bin"
	"$GOPATH/bin"
	"$HOME/.arkade/bin"
	"$HOME/.asdf/bin"
	"$HOME/.asdf/shims"
	"$HOME/.local/bin"
	"$HOME/utils/bin"
)

for p in $__paths[@]; do
	add_to_path $p
done

unset __paths
