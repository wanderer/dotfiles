# map Del escape sequence to actual Del key in all (n,i) modes to resemble vim instead of vi
bindkey -a '^[[3~' delete-char
# map arrow up/down to control fish-like history substring search and highlighting
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
bindkey '^o' fzf-open
# also map the same keys in 'viins' (INSERT) mode...
bindkey -M viins '^[[A' history-substring-search-up
bindkey -M viins '^[[B' history-substring-search-down
bindkey -M viins '^o' fzf-open
# ...and in 'vicmd' (NORMAL) mode...
bindkey -M vicmd '^[[A' history-substring-search-up
bindkey -M vicmd '^[[B' history-substring-search-down
# ...and as well to K, J, instead of arrow up/down, respectively.
# note these are capital letters, original behaviour of j, k is left unchanged
bindkey -M vicmd 'K' history-substring-search-up
bindkey -M vicmd 'J' history-substring-search-down
bindkey '^\' accept-and-hold
