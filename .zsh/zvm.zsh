# if [[ "$(hostnamectl hostname)" == "sth" ]]; then
# if [[ "$(hostname -s)" == "sth" ]]; then
if [[ "$(lsb_release -i -s)" == "Arch" ]]; then
  local fzf_bindings="/usr/share/fzf/key-bindings.zsh"
elif [[ "$(lsb_release -i -s)" == "Fedora" ]]; then
  local fzf_bindings="/usr/share/fzf/shell/key-bindings.zsh"
fi

zvm_after_init_commands+=("[ -f $fzf_bindings ] && source $fzf_bindings")

unset fzf_bindings

function zvm_config() {
  ZVM_LINE_INIT_MODE=$ZVM_MODE_INSERT
  ZVM_CURSOR_STYLE_ENABLED=false
  # ZVM_INSERT_MODE_CURSOR=$ZVM_CURSOR_BLINKING_BLOCK
}
