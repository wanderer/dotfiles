 local dfHome="~/utils/dotfiles"

 alias ls='eza -al --group --group-directories-first'
 alias grubup='sudo grub2-mkconfig -o /boot/grub2/grub.cfg'
 alias am='sudo -i'
 alias mv='mv -i'
 alias cp='cp -i'
 alias ln='ln -i'
 alias free='free -m -l -t'
 alias c='clear'
 alias hs='history | grep -i'
 # already set in /etc/profile.d/...
 # alias grep='grep --color=auto'
 # alias egrep='egrep --color=auto'
 # alias fgrep='fgrep --color=auto'
 alias neofetch='neofetch --disable "theme" "icons" packages de hostname cpu gpu resolution title'
 #alias reth='sudo ethtool -r enp0s25' # ; ip link set enp0s25 down; ip link set enp0s25 up '
 #alias rwlan='ip link set wlp3s0 down; ip link set wlp3s0 up'
 alias wget='wget -c' # allows to restore progress after interruption
 alias vimrc="vim $dfHome/.vim/vimrc"
 alias zshconfig="vim $dfHome/.zshrc"
 alias zshist="vim ~/.zsh_history"
 alias alltheconfigs='alltheconfigs.sh'
 alias agentssh="ssh-add ~/.ssh/$kee" # add development key on demand
 alias rsync-copy='rsync -avz --progress -h'
 alias rsync-move='rsync -avz --progress -h --remove-source-files'
 alias rsync-synchronize='rsync -avzu --delete --progress -h'
 alias rsync-update='rsync -avzu --progress -h'
 alias tmpdir='nutmp=$(mktemp -d);cd "$nutmp";unset nutmp'
 alias wolm='wol d4:3d:7e:53:c2:b1'
 alias ta='tmux a -t0'
 alias dc='docker-compose'
 alias tmux='TERM=xterm-256color tmux'
 alias urlencode='python2 -c "import urllib, sys; print urllib.quote_plus(  sys.argv[1] if len(sys.argv) > 1 else sys.stdin.read()[0:-1], \"\")"'
 alias urldecode='python2 -c "import urllib, sys; print urllib.unquote_plus(sys.argv[1] if len(sys.argv) > 1 else sys.stdin.read()[0:-1])"'
 alias obs="QT_QPA_PLATFORM=xcb obs"
 alias diff="diff -Naru --color"
 alias ip='ip -color'
 alias nmcs='nmcli conn show'
 alias subvols='sudo btrfs subvolume list /home | grep -v -e ".snapshots" -e "${USER}/.local/share/containers" -e "${USER}/.docker" | grep ${USER}'
 alias keyz='ssh-add -l'
 alias gitagain='GIT_COMMITTER_DATE="$(git show -s --format=%ci HEAD)" git commit -v --amend'
[ -z "$IN_NIX_SHELL" ] && {
	if command -v nixGLIntel &> /dev/null; then
		alias gor='nixGLIntel go run'
	fi
}
 alias signal-desktop='signal-desktop --use-tray-icon --enable-features=UseOzonePlatform --ozone-platform=wayland'
 alias dotfiles="cd $dfHome"
 alias whoasn='function(){whois -h whois.cymru.com "-v $1"}'
 alias -g VV=' |& vim -'
 alias -g LL=' |& less'
 alias kubectl='kubectl --insecure-skip-tls-verify'
 alias k='kubectl'
 alias normalise-unicode="python -c \"import unicodedata; import sys; arg=sys.argv[1]; print(unicodedata.normalize('NFKD', arg))\""
 alias attic='nix run github:zhaofengli/attic#default'
