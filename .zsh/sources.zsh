local __tosrc=(
    ~/.zsh/bindings.zsh
    ~/.zsh/fzf.zsh
    ~/.zsh/zvm.zsh

    ~/.dotenv
    ${XDG_RUNTIME_DIR}/secrets/envs
    ~/.zsh/aliases.zsh
    ~/.zsh/functions.zsh
)

for s in $__tosrc[@]; do
    test -f $s && source $s
done

unset __tosrc
