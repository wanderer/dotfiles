function transfer() { if [ $# -eq 0 ]; then echo -e "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md"; return 1; fi;tmpfile=$( mktemp -t transferXXX ); if tty -s; then basefile=$(basename "$1" | sed -e 's/[^a-zA-Z0-9._-]/-/g'); curl --progress-bar --upload-file "$1" "https://transfer.sh/$basefile" >> $tmpfile; else curl --progress-bar --upload-file "-" "https://transfer.sh/$1" >> $tmpfile ; fi; cat $tmpfile; rm -f $tmpfile; }


function info() { printf "\n%s %s\n\n" "$( date )" "$*" >&2; }


function ix () { curl -n -F 'f:1=<-' http://ix.io 2>/dev/null ;}


function viman() { text=$(man "$@") && echo "$text" | vim -R +":set ft=man nomod nonu noma nolist colorcolumn=" - ; }


function upload() {
  # inspired by https://www.alexander-pluhar.de/paste-file-host.html
  # make eternity the default lifespan
  FILE_LIFESPAN=${2:-e}
  FILE_ID=$(base64 /dev/urandom | tr -d '/+' | dd bs=16 count=1 2>/dev/null)
  FILENAME=$(basename $1)

  rsync -auvP -4 $1 "nebula:/var/www/htdocs/dotya.ml/files/$FILE_LIFESPAN/$FILE_ID-$FILENAME"

  FILE_URL="https://dotya.ml/files/$FILE_LIFESPAN/$FILE_ID-$FILENAME"

  if [[ -z "$WAYLAND_DISPLAY" ]]; then
    echo $FILE_URL | xclip -selection clipboard
    echo $FILE_URL | xsel --clipboard --input
  else
    wl-copy --trim-newline $FILE_URL
  fi

  echo $FILE_URL
}


function timesh() {
  shell=${1:-$SHELL}
  for i in $(seq 1 10); do time $shell -i -c exit; done
}

function timeplugins() {
  for plugin ($plugins); do
    timer=$(($(date +%s%N)/1000000))
    if [ -f $ZSH/plugins/$plugin/$plugin.plugin.zsh ]; then
      source $ZSH/plugins/$plugin/$plugin.plugin.zsh
    fi
    now=$(($(date +%s%N)/1000000))
    elapsed=$(($now-$timer))
    echo $elapsed":" $plugin;
  done
}


function delete-branches() {
  git branch |
    grep --invert-match '\*' |
    cut -c 3- |
    fzf --multi --preview="git log {} --" |
    xargs --no-run-if-empty git branch --delete --force
}

function fzf-open() {
  local output
  output=$(fzf </dev/tty)
  if [ $output != "" ]; then
    vim --not-a-term ${output}
  fi
  zle .reset-prompt
}

zle -N fzf-open
