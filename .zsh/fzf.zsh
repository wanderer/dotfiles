export FZF_DEFAULT_COMMAND="fd --type f --hidden --follow --exclude 'node_modules' --exclude 'nix/profiles' ."

# export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_CTRL_T_COMMAND="fd --follow --full-path --hidden --base-directory . --exclude '.git' --exclude 'node_modules' --exclude 'nix/profiles' ."
export FZF_ALT_C_COMMAND="fd -H -t d ."
export DISABLE_FZF_AUTO_COMPLETION="true"
# Options to fzf command
export FZF_COMPLETION_OPTS='+c -x'
export FZF_DEFAULT_OPTS="--height=60% --inline-info --bind=ctrl-j:preview-half-page-down,ctrl-k:preview-half-page-up --color=fg:#f8f8f2,bg:#282a36,hl:#bd93f9 --color=fg+:#f8f8f2,bg+:#44475a,hl+:#bd93f9 --color=info:#ffb86c,prompt:#50fa7b,pointer:#ff79c6 --color=marker:#ff79c6,spinner:#ffb86c,header:#6272a4"
# Using highlight (http://www.andre-simon.de/doku/highlight/en/highlight.html)
# export FZF_CTRL_T_OPTS="--preview '(highlight -O ansi -l {} 2> /dev/null || cat {} || tree -C {}) 2> /dev/null | head -200'"
export FZF_CTRL_T_OPTS="--preview 'bat --style=numbers --color=always --line-range :200 {}'"
