# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

zstyle ':omz:update' mode disabled
zstyle ':omz:update' frequency 3  # days

# Uncomment the following line to disable colors in ls.
DISABLE_LS_COLORS="true"

DISABLE_MAGIC_FUNCTIONS="false"

# Uncomment the following line to disable auto-setting terminal title.
DISABLE_AUTO_TITLE="false"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="false"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
DISABLE_UNTRACKED_FILES_DIRTY="true"

MAILCHECK=0

# allow tab completion in the middle of a word
setopt COMPLETE_IN_WORD
# keep backgroud processess at full speed
setopt NOBGNICE

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="yyyy-mm-dd"
HISTSIZE=10000000
SAVEHIST=10000000
setopt inc_append_history
setopt extended_history # write the history file in the ":start:elapsed;command" format
setopt share_history
setopt hist_expire_dups_first # expire duplicate entries first when trimming history
setopt hist_ignore_dups
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_ignore_space
setopt hist_reduce_blanks # remove superfluous blanks before recording entry
setopt hist_verify # don't execute immediately after expansion
# HISTCONTROL=ignoredups:erasedups

# don't expand aliases _before_ completion has finished
# like: git comm-[tab]
# setopt complete_aliases  # THIS HAS ISSUES

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

plugins=(
	git
	gitignore
	golang
	safe-paste
	ssh-agent
	systemd
	terraform
	lol
	colored-man-pages
)

source $ZSH/oh-my-zsh.sh
