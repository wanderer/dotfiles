export GOPATH=$HOME/go
export CGO_ENABLED="1"

# make home-manager not manage the shell configuration
HMVARSFILE="$HOME/.nix-profile/etc/profile.d/hm-session-vars.sh"
if [ -f "$HMVARSFILE" ]; then . "$HMVARSFILE"; fi


local __paths=(
	"/usr/local/goroot/bin"
	"$HOME/.asdf/shims"
	"$HOME/.arkade/bin"
	"$HOME/.cargo/bin"
	"$GOPATH/bin"
	"/opt/metasploit-framework/bin"
	"$HOME/.local/share/flatpak/exports/bin"
	"/var/lib/flatpak/exports/bin"
	"$HOME/.local/share/JetBrains/Toolbox/bin"
	"$HOME/.local/share/JetBrains/Toolbox/scripts"
)

for p in $__paths[@]; do
	add_to_path $p
done
unset __paths
test -d /run/current-system/sw/bin && add_to_path "/run/current-system/sw/bin"
test -d $HOME/.nix-profile/bin && add_to_path "$HOME/.nix-profile/bin"


export EDITOR='vim'
export TERM='xterm-256color'
export kee="$HOSTNAME"
export SSH_KEY_PATH="$HOME/.ssh/$kee"

if [ ! -S ~/.ssh/ssh_auth_sock ]; then
	eval `ssh-agent -t4h` > /dev/null
	ln -sf "$SSH_AUTH_SOCK" ~/.ssh/ssh_auth_sock
fi


export SSH_AUTH_SOCK=$HOME/.ssh/ssh_auth_sock

if ! [ -f /run/.containerenv  ]; then
	pgrep -x -U "$(id -u)" gnome-keyring-d || export $(gnome-keyring-daemon --start --components=pkcs11,secrets,ssh)
fi

export DOTNET_CLI_TELEMETRY_OPTOUT=1 # why do I always have to take care of this myself...

export ANDROID_SDK_ROOT="$HOME/utils/Android/Sdk"
export ANDROID_SDK_PATH=$ANDROID_SDK_ROOT
export ANDROID_NDK_PATH=$ANDROID_SDK_ROOT/ndk-bundle
export ANDROID_NDK_HOME=$ANDROID_NDK_PATH
export AndroidSdkPath=$ANDROID_SDK_ROOT
export AndroidNdkPath=$ANDROID_NDK_PATH
add_to_path "$ANDROID_SDK_ROOT/tools"
add_to_path "$ANDROID_SDK_ROOT/platform-tools"

unfunction add_to_path


if [[ -z $DISPLAY ]]; then
	export QT_QPA_PLATFORMTHEME=wayland
	# proper theme support?
	# export QT_QPA_PLATFORMTHEME=qt5ct
	export QT_QPA_PLATFORM=wayland
	export QT_AUTO_SCREEN_SCALE_FACTOR=0
	export MOZ_DBUS_REMOTE=1
	export MOZ_USE_XINPUT2=1
	export _JAVA_AWT_WM_NONREPARENTING=1
	export KITTY_ENABLE_WAYLAND=1
	export BEMENU_BACKEND=wayland
	export SDL_VIDEODRIVER=wayland
	export NO_AT_BRIDGE=1
	export TDESKTOP_USE_PORTAL=1
	export XDG_SESSION_TYPE=wayland
	if [[ "${HOSTNAME}" == "surtur" ]]; then
		export LIBVA_DRIVER_NAME="i965"
	fi

	test -f ~/.zsh/bemenu-dracula/bemenu-dracula && . ~/.zsh/bemenu-dracula/bemenu-dracula

	# if [[ $(tty) == /dev/tty1 ]]; then
	if [ -z "${WAYLAND_DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
		export GTK_IM_MODULE=ibus
		export QT_IM_MODULE=ibus
		export XMODIFIERS=@im=ibus
		export SAL_USE_VCLPLUGIN=gtk3 # for libreoffice
		export XDG_CURRENT_DESKTOP=sway
		if [[ "${HOSTNAME}" == "leo" ]]; then
			# needs vulkan libs.
			export WLRRENDERER=vulkan
		fi
		# exec systemd-cat --identifier=sway sway > /var/tmp/sway.log 2>&1
		sway -d -V >> /var/tmp/sway.log 2>&1
	elif [ "${XDG_VTNR}" -eq 2 ]; then
		export MOZ_ENABLE_WAYLAND=1
		export XDG_SESSION_TYPE=wayland
		exec systemd-cat --identifier=gnome-session gnome
	fi
fi
