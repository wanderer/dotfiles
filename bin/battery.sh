#!/bin/bash

# courtesy of https://www.reddit.com/r/gnome/comments/qn60lo/automatic_power_profile_selection/

capacity=$(cat /sys/class/power_supply/BAT0/capacity)
old_capacity=$(cat /tmp/bat_capacity)
[[ "$capacity" == "$old_capacity" ]] && exit 0

echo $capacity > /tmp/bat_capacity

eval "$(cat /sys/class/power_supply/BAT0/uevent|sed -e 's/ //g')"

if [[ "$POWER_SUPPLY_STATUS" == "Discharging" ]]
then
	if (( $POWER_SUPPLY_CAPACITY > 30 ))
	then
		powerprofilesctl set balanced
	else
		powerprofilesctl set power-saver
	fi
else
	powerprofilesctl set performance
fi
