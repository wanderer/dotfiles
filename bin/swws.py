#!/usr/bin/python
# thanks to dalahast < https://git.dotya.ml/dalahast >

import json,sys,subprocess

args = sys.argv
if len(args) > 2:
    a = args[2]
else:
    a = ""
ws = args[1]
ws10 = str(10+int(ws))
monnum = (subprocess.run(["bash", "-c", "swaymsg -t get_outputs| grep type | grep output | wc -l|xargs"],stdout=subprocess.PIPE)).stdout.decode('utf-8').rstrip()
clamshell = (subprocess.run(["bash", "-c", "swaymsg -t get_outputs| grep -A1 eDP | grep -c 'false' |xargs"],stdout=subprocess.PIPE)).stdout.decode('utf-8').rstrip()
if (monnum == "2"):
    if(clamshell == "1"):
        ws10 = ws
        ws20 = ws
    else:
        ws20 = ws10
else:
    ws20 = str(20+int(ws))

msg = json.loads(subprocess.run(["swaymsg", "-t", "get_workspaces"], stdout=subprocess.PIPE).stdout.decode('utf-8'))

def expl_pick(mon_type):
    if mon_type == "eDP-1":
        subprocess.run(["swaymsg", "focus", "output", "HDMI-A-1"])
        subprocess.run(["swaymsg", "workspace", ws10])
    else:
        subprocess.run(["swaymsg", "focus", "output", "eDP-1"])
        subprocess.run(["swaymsg", "workspace", ws])

def mov_to(mon_type):
    if mon_type == "eDP-1":
        subprocess.run(["swaymsg", "move", "container", "to", "workspace", ws])
    else:
        subprocess.run(["swaymsg", "move", "container", "to", "workspace", ws10])

def mov_to_next_mon(mon_type):
    if mon_type == "eDP-1":
        subprocess.run(["swaymsg", "move", "container", "to", "workspace", ws10])
    else:
        subprocess.run(["swaymsg", "move", "container", "to", "workspace", ws])

def wssw(mon_type):
    if a == '-c':
        expl_pick(mon_type)
    elif a == '-s':
        mov_to(mon_type)
    elif a == '-t':
        mov_to_next_mon(mon_type)

for i in range(len(msg)):
    if msg[i]["focused"] == True:
        if msg[i]["output"] == "eDP-1":
            if not a:
                subprocess.run(["swaymsg", "workspace", ws])
            else:
                mon_type = "eDP-1"
                wssw(mon_type)
        if msg[i]["output"] == "HDMI-A-1":
            if not a:
                subprocess.run(["swaymsg", "workspace", ws10])
            else:
                mon_type = "HDMI-A-1"
                wssw(mon_type)
        if msg[i]["output"] == "DP-2":
            if not a:
                subprocess.run(["swaymsg", "workspace", ws10])
            else:
                mon_type = "DP-2"
                wssw(mon_type)
        if msg[i]["output"] == "DP-3":
            if not a:
                subprocess.run(["swaymsg", "workspace", ws20])
            else:
                mon_type = "DP-3"
                wssw(mon_type)
        if msg[i]["output"] == "DP-4":
            if not a:
                subprocess.run(["swaymsg", "workspace", ws10])
            else:
                mon_type = "DP-4"
                wssw(mon_type)
        if msg[i]["output"] == "DP-5":
            if not a:
                subprocess.run(["swaymsg", "workspace", ws10])
            else:
                mon_type = "DP-5"
                wssw(mon_type)
