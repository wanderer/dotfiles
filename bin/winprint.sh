#!/bin/bash

# taken from https://github.com/swaywm/sway/blob/12e223e7973f7d7132d95c6302328067dec732ce/contrib/grimshot
FOCUSED=$(swaymsg -t get_tree | jq -r 'recurse(.nodes[]?, .floating_nodes[]?) | select(.focused)')
GEOM=$(echo "$FOCUSED" | jq -r '.rect | "\(.x),\(.y) \(.width)x\(.height)"')
grim -g "$GEOM" - | wl-copy
