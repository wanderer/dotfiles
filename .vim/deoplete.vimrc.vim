let g:deoplete#enable_at_startup = 0
let g:deoplete#disable_auto_complete = 1
inoremap <expr> <C-n>  deoplete#manual_complete()
let g:deoplete#enable_ignore_case = 1
" Use head matcher instead of fuzzy matcher
" call deoplete#custom#source('_',
" \ 'matchers', ['matcher_head'])
call deoplete#custom#option({
\ 'auto_complete_delay': 230,
\ 'auto_refresh_delay': 240,
\ 'refresh_always': v:true,
\ 'on_insert_enter': v:true,
\ 'enable_slash_completion': v:true,
\ 'enable_auto_completion': v:false,
\ 'smart_case': v:true,
\ })
call deoplete#custom#option('skip_chars', ['(', ')', ':'])
" call deoplete#custom#option('keyword_patterns', {
" \ '_': '[a-zA-Z_]\k*',
" \ 'tex': '\\?[a-zA-Z_]\w*',
" \})
" call deoplete#custom#option('omni_patterns', {
" \ 'cpp': ['[^. *\t]\%(\.\|->\)\w*', '[a-zA-Z_]\w*::'],
" \})
let g:deoplete#sources#sort_class = ['type', 'package', 'func', 'var', 'const']
let g:deoplete#omni_patterns = {}
let g:deoplete#omni_patterns.go = '[^. *\t]\.\w*'
let g:deoplete#omni_patterns.cpp = ['[^. *\t]\%(\.\|->\)\w*', '[a-zA-Z_]\w*::']
" let g:deoplete#min_pattern_length = 1
let g:deoplete#min_pattern_length = 0
let g:deoplete#auto_completion_start_length = 0
call deoplete#custom#option('sources', {
\ '_': ['ale', 'buffer', 'around'],
\ 'cpp': ['ale', 'tag', 'lsp#complete', 'lsp', 'buffer', 'around', 'ultisnips'],
\ 'cmake': ['buffer', 'tag', 'ale', 'lsp#complete', 'lsp', 'ultisnips'],
\ 'go': ['ale', 'tag', 'lsp#complete', 'lsp', 'buffer', 'around'],
\ 'py': ['buffer', 'tag', 'ale', 'lsp#complete', 'lsp', 'ultisnips'],
\ 'python': ['buffer', 'tag', 'ale', 'jedi', 'lsp#complete', 'lsp', 'ultisnips'],
\})
" \ 'cpp': ['buffer', 'tag', 'ale', 'lsp_clangd', 'lsp#complete', 'lsp', 'ultisnips'],
" \ 'go': ['ale', 'tag', 'lsp#complete', 'lsp', 'buffer', 'around', 'ultisnips'],

call deoplete#custom#option('ignore_sources', ['lsp_clangd'])

" set rank of a particular source
call deoplete#custom#source('ultisnips', 'rank', 101)
" call deoplete#custom#source('lsp_clangd', 'rank', 104)
" call deoplete#custom#source('lsp_clangd',
" \ 'filetypes', ['c', 'cpp'])
call deoplete#custom#source('lsp#complete', 'rank', 102)
call deoplete#custom#source('buffer', 'rank', 10)
call deoplete#custom#source('around', 'rank', 10)
call deoplete#custom#source('lsp', 'rank', 102)
call deoplete#custom#source('ale', 'rank', 11)
" call deoplete#custom#option('profile', v:true)
" call deoplete#enable_logging('DEBUG', 'deoplete.log')


" clang
let g:deoplete#sources#clang#libclang_path='/usr/lib64/libclang.so'
let g:deoplete#sources#clang#clang_header='/usr/include/clang'
let g:deoplete#sources#clang#std = {'c': 'c11', 'cpp': 'c++20'}
let g:deoplete#sources#clang#filter_availability_kinds = ['NotAvailable', 'NotAccessible']


" python deoplete
let g:deoplete#sources#jedi#enable_typeinfo = 1


" rust deoplete
let g:deoplete#sources#rust#rust_source_path='/usr/bin/rust'
let g:deoplete#sources#rust#show_duplicates=0
